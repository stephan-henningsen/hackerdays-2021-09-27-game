package com.trifork.hackerdays.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.trifork.hackerdays.game.SpaceInvadersGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = SpaceInvadersGame.GAME_WIDTH * 3;
		config.height = SpaceInvadersGame.GAME_HEIGHT * 3;
		config.title = "Space Invaders";
		config.resizable = false;
		new LwjglApplication(new SpaceInvadersGame(), config);
	}
}
