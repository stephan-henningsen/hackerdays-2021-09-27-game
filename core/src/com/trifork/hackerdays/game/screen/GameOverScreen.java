package com.trifork.hackerdays.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.ScreenUtils;
import com.trifork.hackerdays.game.SpaceInvadersGame;

public class GameOverScreen extends AbstractScreen
{

    private float cooldownRemaining = .800f;



    public GameOverScreen ( SpaceInvadersGame game )
    {
        super(game);
    }

    @Override
    public void render ( float dt )
    {
        ScreenUtils.clear(0, 0, 0, 1);

        game.batch.begin();

        game.font.draw(game.batch, "GAME OVER!", 45, 200);
        game.font.draw(game.batch, "LEVEL " + game.level, 55, 140);
        game.font.draw(game.batch, "SCORE " + game.score, 55, 130);
        game.font.draw(game.batch, "HI-SCORE " + game.highScore, 40, 120);

        game.font.draw(game.batch, "PRESS SPACEBAR",  25, 60);
        game.font.draw(game.batch, "TO CONTINUE",     35, 50);

        game.batch.end();

        if ( cooldownRemaining > 0 ) {
            cooldownRemaining -= dt;
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.SPACE) && cooldownRemaining <= 0 ) {
            game.setScreen(new TitleScreen(game));
            dispose();
        }
    }
}
