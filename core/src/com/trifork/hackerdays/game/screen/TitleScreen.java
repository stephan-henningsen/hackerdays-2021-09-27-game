package com.trifork.hackerdays.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.ScreenUtils;
import com.trifork.hackerdays.game.SpaceInvadersGame;

public class TitleScreen extends AbstractScreen
{

    private float cooldownRemaining = .800f;



    public TitleScreen ( SpaceInvadersGame game )
    {
        super(game);
    }

    @Override
    public void show ()
    {
        super.show();
        game.score = 0;
        game.level = 1;
        game.lives = 3;
    }

    @Override
    public void render ( float dt )
    {
        ScreenUtils.clear(0, 0, 0, 1);

        game.batch.begin();
        game.font.draw(game.batch, "SCORE\n" + game.score, SpaceInvadersGame.BORDER, SpaceInvadersGame.GAME_HEIGHT - game.font.getCapHeight());
        game.font.draw(game.batch, "HI-SCORE\n" + game.highScore, SpaceInvadersGame.GAME_WIDTH - 70, SpaceInvadersGame.GAME_HEIGHT - game.font.getCapHeight());

        game.font.draw(game.batch, "SPACE INVADERS!", 25, 180);

        game.font.draw(game.batch, "PRESS SPACEBAR",  25, 60);
        game.font.draw(game.batch, "TO CONTINUE",     35, 50);
        game.batch.end();

        if ( cooldownRemaining > 0 ) {
            cooldownRemaining -= dt;
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.SPACE) && cooldownRemaining <= 0 ) {
            game.setScreen(new PlayScreen(game));
            dispose();
        }
    }
}
