package com.trifork.hackerdays.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.trifork.hackerdays.game.SpaceInvadersGame;
import com.trifork.hackerdays.game.sprite.Alien;
import com.trifork.hackerdays.game.sprite.Laser;
import com.trifork.hackerdays.game.sprite.Missile;
import com.trifork.hackerdays.game.sprite.Player;
import com.trifork.hackerdays.game.sprite.cache.TextureCache;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PlayScreen extends AbstractScreen
{
    private static final int PLAYER_SPEED = 100;
    private static final float PLAYER_COOLDOWN = .250f;

    private List<Alien> aliens;
    private Player player;
    private List<Missile> missiles;
    private List<Laser> lasers;
    private boolean debug = false;

    private float keyPressCooldownRemaining = PLAYER_COOLDOWN;
    private float alienCooldownRemaining = 5;
    private float alienSpeedX = Alien.SPEED;

    public PlayScreen ( SpaceInvadersGame game )
    {
        super(game);
    }

    @Override
    public void show ()
    {
        aliens = createAliens();
        player = game.spriteFactory.createPlayer(10, 10);
        missiles = new ArrayList<>();
        lasers = new ArrayList<>();
    }

    private List<Alien> createAliens ()
    {
        List<Alien> aliens = new ArrayList<>();

        int cols = 11;
        int rows = 5;

        for ( int row = 0; row < rows; row++ ) {
            for ( int col = 0; col < cols; col++ ) {
                int x = SpaceInvadersGame.BORDER + col * (TextureCache.ALIEN_WIDTH + 4);
                int y = SpaceInvadersGame.GAME_HEIGHT - SpaceInvadersGame.TOP - row * (TextureCache.ALIEN_HEIGHT + 4);
                switch ( row ) {
                    case 0:
                        aliens.add(game.spriteFactory.createAlien1(x, y));
                        break;

                    case 1:
                    case 2:
                        aliens.add(game.spriteFactory.createAlien2(x, y));
                        break;

                    case 3:
                    case 4:
                        aliens.add(game.spriteFactory.createAlien3(x, y));
                        break;
                }
            }
        }

        Collections.reverse(aliens);  // We want Aliens lower rows closer to Player to be considered first in collision detection etc.

        return aliens;
    }

    @Override
    public void render ( float dt )
    {
        update(dt);
        action(dt);
        input(dt);
        draw(dt);
    }

    private void update ( float dt )
    {
        for ( Alien alien : aliens ) {
            alien.update(dt);
        }

        player.update(dt);

        moveMissileAndKillAlien(dt);
        moveLasersAndKillPlayer(dt);
        moveAliensAndKillPlayer(dt);

        if ( keyPressCooldownRemaining > 0 ) {
            keyPressCooldownRemaining -= dt;
        }

        if ( aliens.isEmpty() ) {
            onWin();
        }

        if ( player.isRemovable()) {
            onGameOver();
        }
    }

    private void moveMissileAndKillAlien ( float dt )
    {
        for ( Iterator<Missile> missileIt = missiles.listIterator(); missileIt.hasNext(); ) {
            Missile missile = missileIt.next();
            missile.update(dt);

            if ( missile.isAbove(SpaceInvadersGame.GAME_HEIGHT) ) {
                missileIt.remove();
                continue; // Missile removed, it cannot kill any Enemies now.
            }

            for ( Iterator<Alien> alienIt = aliens.listIterator(); alienIt.hasNext(); ) {
                Alien alien = alienIt.next();
                if ( alien.isAlive() && missile.isColliding(alien) ) {
                    missileIt.remove();
                    alien.die();
                    game.score += alien.getPoints();
                    break; // Missile cannot kill more than one Enemy anyway.
                }
            }
        }
    }

    private void moveLasersAndKillPlayer ( float dt )
    {
        for ( Iterator<Laser> laserIt = lasers.listIterator(); laserIt.hasNext(); ) {
            Laser laser = laserIt.next();
            laser.update(dt);

            if ( !laser.isAbove(0) ) {
                laserIt.remove();
                continue;
            }

            if ( laser.isColliding(player) ) {
                laserIt.remove();
                takeDamage();
                checkForGameOver();
                onPlayerKilled();
            }
        }
    }

    private void onPlayerKilled ()
    {
        player.die();
    }

    private void moveAliensAndKillPlayer ( float dt )
    {
        for ( Iterator<Alien> alienIt = aliens.listIterator(); alienIt.hasNext(); ) {
            Alien alien = alienIt.next();
            if ( !alien.isAlive() && alien.isRemovable() ) {
                alienIt.remove();
                if ( aliens.size() == 8 || aliens.size() == 4 || aliens.size() == 2 ) {
                    alienSpeedX *= 4;
                    alien.divWalkStepDuration(4);
                }
                continue;
            }

            alien.addX(alienSpeedX * dt);
        }

        if ( isAtScreenEdge() ) {
            alienSpeedX = -alienSpeedX;
            for ( Alien alien : aliens ) {
                alien.addY(-SpaceInvadersGame.ROW_HEIGHT); // Move one row down.

                if ( alien.isColliding(player) || !alien.isAbove(0) ) {
                    takeDamage();
                    checkForGameOver();
                    onPlayerKilled();
                }
            }
        }
    }

    private void takeDamage() {
        game.lives--;
    }

    private void checkForGameOver() {
        if (game.lives <= 0)
            onGameOver();
    }

    private boolean isAtScreenEdge ()
    {
        for ( Alien alien : aliens ) {
            if ( alienSpeedX > 0 ) {
                int screenEdge = SpaceInvadersGame.GAME_WIDTH - SpaceInvadersGame.BORDER;
                float alienEdge = alien.getX() + alien.getWidth();
                if ( alienEdge >= screenEdge ) {
                    return true;
                }
            } else if ( alienSpeedX < 0 ) {
                int screenEdge = SpaceInvadersGame.BORDER;
                float alienEdge = alien.getX();
                if ( alienEdge <= screenEdge ) {
                    return true;
                }
            }
        }
        return false;
    }



    private void input ( float dt )
    {
        if ( player.isAlive() ) {
            if ( Gdx.input.isKeyPressed(Input.Keys.RIGHT) ) {
                player.moveRight(PLAYER_SPEED * dt, SpaceInvadersGame.GAME_WIDTH - player.getWidth());
            }
            if ( Gdx.input.isKeyPressed(Input.Keys.LEFT) ) {
                player.moveLeft(-PLAYER_SPEED * dt, 0);
            }
            if ( Gdx.input.isKeyPressed(Input.Keys.SPACE) && isKeyPressAllowed() ) {
                Missile missile = game.spriteFactory.createMissile(player.getX() + player.getWidth() / 2f,
                                                                   player.getY() + TextureCache.MISSILE_HEIGHT);
                missiles.add(missile);
            }
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.N) && isKeyPressAllowed() ) {
            List<Alien> copy = new ArrayList<>(aliens);
            Collections.shuffle(copy);
            int nuke = (copy.size() - 1) / 2;
            nuke = Math.max(1, nuke);
            for ( int n = 0; n < nuke; n++ ) {
                Alien alien = copy.get(n);
                alien.die();
            }
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.W) && isKeyPressAllowed() ) {
            onWin();
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.R) && isKeyPressAllowed() ) {
            game.setScreen(new PlayScreen(game));
            dispose();
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.D) && isKeyPressAllowed() ) {
            debug = !debug;
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.K) && isKeyPressAllowed() ) {
            onPlayerKilled();
        }

        if ( Gdx.input.isKeyPressed(Input.Keys.H) && isKeyPressAllowed() ) {
            game.prefs.putInteger("highscore", 0);
            game.prefs.flush();
        }
    }

    private boolean isKeyPressAllowed ()
    {
        if ( keyPressCooldownRemaining <= 0 ) {
            keyPressCooldownRemaining = PLAYER_COOLDOWN;
            return true;
        }
        return false;
    }


    private void action ( double dt )
    {
        alienCooldownRemaining -= dt;

        if ( alienCooldownRemaining <= 0 ) {
            int max = aliens.size() - 1;
            int r = (int)(max * Math.random());
            Alien alien = aliens.get(r);
            switch ( alien.getType() ) {
                case 1:
                    lasers.add(game.spriteFactory.createLaser1(alien.getX(), alien.getY()));
                    break;
                case 2:
                    lasers.add(game.spriteFactory.createLaser2(alien.getX(), alien.getY()));
                    break;
                case 3:
                    lasers.add(game.spriteFactory.createLaser3(alien.getX(), alien.getY()));
                    break;
            }
            alienCooldownRemaining = (float)(.5f + Math.random() * 3f);
        }
    }



    private void draw ( float dt )
    {
        if ( debug ) {
            ScreenUtils.clear(0.5f, 0.1f, 0.3f, 1);
        } else {
            ScreenUtils.clear(0, 0, 0, 1);
        }

        game.batch.begin();

        game.font.draw(game.batch, "SCORE:\n" + game.score, SpaceInvadersGame.BORDER, SpaceInvadersGame.GAME_HEIGHT - game.font.getCapHeight());

        game.font.draw(game.batch, "HI-SCORE:\n" + game.highScore, SpaceInvadersGame.GAME_WIDTH - 70, SpaceInvadersGame.GAME_HEIGHT - game.font.getCapHeight());

        for ( Alien alien : aliens ) {
            game.batch.draw(alien.getTextureRegion(), alien.getX(), alien.getY());
        }

        game.batch.draw(player.getTextureRegion(), player.getX(), player.getY());

        for ( Missile missle : missiles ) {
            game.batch.draw(missle.getTextureRegion(), missle.getX(), missle.getY());
        }

        for ( Laser laser : lasers ) {
            game.batch.draw(laser.getTextureRegion(), laser.getX(), laser.getY());
        }

        game.font.draw(game.batch, "" + game.lives, 4, 8);

        game.batch.end();
    }



    private void onWin ()
    {
        game.setScreen(new WinScreen(game));
        dispose();
    }

    private void onGameOver ()
    {
        if (game.score > game.highScore){
            game.highScore = game.score;
            game.prefs.putInteger("highscore", game.highScore);
            game.prefs.flush();
        }
        game.setScreen(new GameOverScreen(game));
        dispose();
    }
}
