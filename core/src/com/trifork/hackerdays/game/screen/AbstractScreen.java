package com.trifork.hackerdays.game.screen;

import com.badlogic.gdx.Screen;
import com.trifork.hackerdays.game.SpaceInvadersGame;

public abstract class AbstractScreen implements Screen
{

    protected final SpaceInvadersGame game;



    public AbstractScreen ( SpaceInvadersGame game )
    {
        this.game = game;
    }



    @Override
    public void show ()
    {
    }

    @Override
    public void render ( float dt )
    {
    }

    @Override
    public void resize ( int width, int height )
    {
    }

    @Override
    public void pause ()
    {
    }

    @Override
    public void resume ()
    {
    }

    @Override
    public void hide ()
    {
    }

    @Override
    public void dispose ()
    {
    }
}
