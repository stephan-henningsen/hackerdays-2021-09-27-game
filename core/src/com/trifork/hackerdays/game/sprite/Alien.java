package com.trifork.hackerdays.game.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.trifork.hackerdays.game.sprite.cache.AlienTextures;

public class Alien extends Sprite
{

    public static final int SPEED = 1;

    private static final float WALK_STEP_DURATION = SPEED;
    private static final float DEATH_DURATION = 0.25f;



    private final Animation walkAnimation;
    private final TextureRegion deadTexture;

    private final int type;
    private final int points;

    private float deadDurationElapsed = 0;

    private boolean alive = true;
    private boolean removable = false;



    public Alien ( AlienTextures t, float x, float y, int width, int height, int type, int points )
    {
        super(x, y, width, height);
        walkAnimation = new Animation(t.walk1, WALK_STEP_DURATION, t.walk2, WALK_STEP_DURATION);
        deadTexture = t.dead;
        this.type = type;
        this.points = points;
    }

    @Override
    public void update ( float dt )
    {
        if ( !removable ) {
            if ( alive ) {
                walkAnimation.update(dt);
            } else {
                deadDurationElapsed += dt;
                if ( deadDurationElapsed > DEATH_DURATION ) {
                    removable = true;
                }
            }
        }
    }

    @Override
    public TextureRegion getTextureRegion ()
    {
        if ( alive ) {
            return walkAnimation.getFrameTextureRegion();
        } else {
            return deadTexture;
        }
    }

    public void divWalkStepDuration ( float div )
    {
        walkAnimation.divDurations( div );
    }

    public int getType ()
    {
        return type;
    }

    public int getPoints ()
    {
        return points;
    }

    public void die ()
    {
        alive = false;
        deadDurationElapsed = 0;
    }

    public boolean isAlive ()
    {
        return alive;
    }

    public boolean isRemovable ()
    {
        return removable;
    }



    //
    //            A-----+                  +-----A
    //            |     |                  |     |
    //            |  +--+--+           +---+-+   |
    //            +--+--+  |           |   +-+---+
    //               |     |           |     |
    //               +-----P           P-----+
    //
    public boolean isColliding ( Player player )
    {
        float alienBottom = y;
        float alienTop = alienBottom + height;
        float alienLeft = x;
        float alienRight = alienLeft + width;

        float playerBottom = player.getY();
        float playerTop = playerBottom + player.getHeight();
        float playerLeft = player.getX();
        float playerRight = playerLeft + player.getWidth();

        return alienBottom <= playerTop &&
               ((alienLeft >= playerLeft && alienRight >= playerLeft) ||
                (alienLeft <= playerRight && alienRight >= playerRight));
    }

}
