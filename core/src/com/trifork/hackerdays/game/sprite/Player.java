package com.trifork.hackerdays.game.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.trifork.hackerdays.game.sprite.cache.PlayerTextures;

public class Player extends Sprite
{
    private static final float DEATH_DURATION = 1f;

    private final TextureRegion cannon;
    private final Animation burnAnimation;

    private boolean alive = true;
    private boolean removable = false;
    private float deadDurationElapsed = 0;

    public Player ( PlayerTextures t, float x, float y, int width, int height )
    {
        super(x, y, width, height);
        cannon = t.player;
        burnAnimation = new Animation(t.burn1, .100f, t.burn2, .200f);
    }

    @Override
    public void update ( float dt )
    {
        if ( !alive ) {
            deadDurationElapsed += dt;
            burnAnimation.update(dt);
            if ( deadDurationElapsed >= DEATH_DURATION ) {
                removable = true;
            }
        }
    }

    @Override
    public TextureRegion getTextureRegion ()
    {
        if ( alive ) {
            return cannon;
        } else {
            return burnAnimation.getFrameTextureRegion();
        }
    }


    public boolean isAlive ()
    {
        return alive;
    }

    public boolean isRemovable ()
    {
        return removable;
    }

    public void die ()
    {
        alive = false;
    }
}
