package com.trifork.hackerdays.game.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.trifork.hackerdays.game.sprite.cache.LaserTextures;

public class Laser extends Sprite
{

    private static final int SPEED = 200;
    private static final float ANIMATION_FRAME_DURATION = .200f;

    private final Animation animation;
    private final TextureRegion hitTexture;


    private float animationFrameDuraiton;
    private int frame = 0;


    public Laser (
        LaserTextures t,
        TextureRegion hitTexture,
        float x,
        float y,
        int width,
        int height )
    {
        super(x, y, width, height);
        animation = new Animation(t.a, ANIMATION_FRAME_DURATION, t.b, ANIMATION_FRAME_DURATION, t.c, ANIMATION_FRAME_DURATION, t.d, ANIMATION_FRAME_DURATION);
        this.hitTexture = hitTexture;
    }

    @Override
    public void update ( float dt )
    {
        y += -SPEED * dt;
        animation.update(dt);
    }

    @Override
    public TextureRegion getTextureRegion ()
    {
        return animation.getFrameTextureRegion();
    }


    //
    //    ^
    //    |
    //  y |
    //    +----->    #
    //      x        #
    //            +--#--+
    //            |  #  |
    //            |     |
    //            +-----+
    //
    public boolean isColliding ( Player player )
    {
        float playerBottom = player.getY();
        float playerTop = playerBottom + player.getHeight();
        float playerLeft = player.getX();
        float playerRight = playerLeft + player.getWidth();

        float laserX = getX() + 1; // Center laser; width is 3.
        float laserBottom = getY();
        float laserTop = laserBottom + getHeight();

        return laserX >= playerLeft && laserX <= playerRight && laserBottom <= playerTop && laserTop >= playerBottom;
    }
}
