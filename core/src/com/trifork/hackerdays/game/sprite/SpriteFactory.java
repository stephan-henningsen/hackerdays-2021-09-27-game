package com.trifork.hackerdays.game.sprite;

import com.trifork.hackerdays.game.sprite.cache.TextureCache;

public class SpriteFactory
{

    private final TextureCache textureCache;



    public SpriteFactory ( TextureCache textureCache )
    {
        this.textureCache = textureCache;
    }



    public Alien createAlien1 ( float x, float y )
    {
        return new Alien(textureCache.getAlienTextures1(), x, y, TextureCache.ALIEN_WIDTH, TextureCache.ALIEN_HEIGHT, 1, 10);
    }

    public Alien createAlien2 ( float x, float y )
    {
        return new Alien(textureCache.getAlienTextures2(), x, y, TextureCache.ALIEN_WIDTH, TextureCache.ALIEN_HEIGHT, 2, 20);
    }

    public Alien createAlien3 ( float x, float y )
    {
        return new Alien(textureCache.getAlienTextures3(), x, y, TextureCache.ALIEN_WIDTH, TextureCache.ALIEN_HEIGHT, 3, 30);
    }



    public Player createPlayer ( float x, float y )
    {
        return new Player(textureCache.getPlayerTextures(), x, y, TextureCache.PLAYER_WIDTH, TextureCache.PLAYER_HEIGHT);
    }



    public Missile createMissile ( float x, float y )
    {
        return new Missile(textureCache.getMissileTexture(),
                           x, y, TextureCache.MISSILE_WIDTH, TextureCache.MISSILE_HEIGHT);
    }



    public Laser createLaser1 ( float x, float y )
    {
        return new Laser(textureCache.getLaserTextures1(), textureCache.getHitTexture(), x, y, TextureCache.LASER_WIDTH, TextureCache.MISSILE_HEIGHT);
    }

    public Laser createLaser2 ( float x, float y )
    {
        return new Laser(textureCache.getLaserTextures2(), textureCache.getHitTexture(), x, y, TextureCache.LASER_WIDTH, TextureCache.MISSILE_HEIGHT);
    }

    public Laser createLaser3 ( float x, float y )
    {
        return new Laser(textureCache.getLaserTextures3(), textureCache.getHitTexture(), x, y, TextureCache.LASER_WIDTH, TextureCache.MISSILE_HEIGHT);
    }

}
