package com.trifork.hackerdays.game.sprite.cache;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class LaserTextures
{
    public final TextureRegion a;
    public final TextureRegion b;
    public final TextureRegion c;
    public final TextureRegion d;

    public LaserTextures ( TextureRegion a, TextureRegion b, TextureRegion c, TextureRegion d )
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
}
