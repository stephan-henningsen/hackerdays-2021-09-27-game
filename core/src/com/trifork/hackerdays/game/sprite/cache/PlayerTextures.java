package com.trifork.hackerdays.game.sprite.cache;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PlayerTextures
{
    public final TextureRegion player;
    public final TextureRegion burn1;
    public final TextureRegion burn2;

    public PlayerTextures ( TextureRegion player, TextureRegion burn1, TextureRegion burn2 )
    {
        this.player = player;
        this.burn1 = burn1;
        this.burn2 = burn2;
    }
}
