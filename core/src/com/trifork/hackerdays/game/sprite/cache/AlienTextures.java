package com.trifork.hackerdays.game.sprite.cache;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AlienTextures
{
   public final TextureRegion walk1;
   public final TextureRegion walk2;
   public final TextureRegion dead;

   public AlienTextures ( TextureRegion walk1, TextureRegion walk2, TextureRegion dead )
   {

      this.walk1 = walk1;
      this.walk2 = walk2;
      this.dead = dead;
   }
}
