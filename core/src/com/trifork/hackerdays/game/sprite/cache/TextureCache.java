package com.trifork.hackerdays.game.sprite.cache;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TextureCache
{

    public static final int ALIEN_WIDTH = 8;
    public static final int ALIEN_HEIGHT = 8;

    public static final int PLAYER_WIDTH = 15;
    public static final int PLAYER_HEIGHT = 8;

    public static final int MISSILE_WIDTH = 1;
    public static final int MISSILE_HEIGHT = 6;

    public static final int LASER_WIDTH = 3;
    public static final int LASER_HEIGHT = 7;

    public static final int HIT_WIDTH = 6;
    public static final int HIT_HEIGHT = 8;

    public static final int GAP = 1;



    private AlienTextures alienTextures1;
    private AlienTextures alienTextures2;
    private AlienTextures alienTextures3;
    private PlayerTextures playerTextures;
    private TextureRegion missileTexture;
    private LaserTextures laserTextures1;
    private LaserTextures laserTextures2;
    private LaserTextures laserTextures3;
    private TextureRegion hitTexture;



    public TextureCache ( Texture spriteSheet )
    {
        alienTextures1 = createAlienTextures1(spriteSheet);
        alienTextures2 = createAlienTextures2(spriteSheet);
        alienTextures3 = createAlienTextures3(spriteSheet);
        playerTextures = createPlayerTexture(spriteSheet);
        missileTexture = createMissileTexture(spriteSheet);
        laserTextures1 = createLaserTextures1(spriteSheet);
        laserTextures2 = createLaserTextures2(spriteSheet);
        laserTextures3 = createLaserTextures3(spriteSheet);
        hitTexture = createHitTexture(spriteSheet);
    }

    private AlienTextures createAlienTextures1 ( Texture spriteSheet )
    {
        TextureRegion walk1 = new TextureRegion(spriteSheet, 0, 0, ALIEN_WIDTH, ALIEN_HEIGHT);
        TextureRegion walk2 = new TextureRegion(spriteSheet, ALIEN_WIDTH+GAP, 0, ALIEN_WIDTH, ALIEN_HEIGHT);
        TextureRegion die = new TextureRegion(spriteSheet, 103, 0, ALIEN_WIDTH, ALIEN_HEIGHT);
        return new AlienTextures(walk1, walk2, die);
    }

    private AlienTextures createAlienTextures2 ( Texture spriteSheet )
    {
        TextureRegion walk1 = new TextureRegion(spriteSheet, 0, 45, ALIEN_WIDTH, ALIEN_HEIGHT);
        TextureRegion walk2 = new TextureRegion(spriteSheet, ALIEN_WIDTH+GAP, 45, ALIEN_WIDTH, ALIEN_HEIGHT);
        TextureRegion die = new TextureRegion(spriteSheet, 103, 36, ALIEN_WIDTH, ALIEN_HEIGHT);
        return new AlienTextures(walk1, walk2, die);
    }

    private AlienTextures createAlienTextures3 ( Texture spriteSheet )
    {
        TextureRegion walk1 = new TextureRegion(spriteSheet, 0, 90, ALIEN_WIDTH, ALIEN_HEIGHT);
        TextureRegion walk2 = new TextureRegion(spriteSheet, ALIEN_WIDTH+GAP, 90, ALIEN_WIDTH, ALIEN_HEIGHT);
        TextureRegion die = new TextureRegion(spriteSheet, 103, 72, ALIEN_WIDTH, ALIEN_HEIGHT);
        return new AlienTextures(walk1, walk2, die);
    }

    private PlayerTextures createPlayerTexture ( Texture spriteSheet )
    {
        TextureRegion player = new TextureRegion(spriteSheet, 113, 63, PLAYER_WIDTH, PLAYER_HEIGHT);
        TextureRegion burn1 = new TextureRegion(spriteSheet, 130, 63, PLAYER_WIDTH, PLAYER_HEIGHT);
        TextureRegion burn2 = new TextureRegion(spriteSheet, 147, 63, PLAYER_WIDTH, PLAYER_HEIGHT);
        return new PlayerTextures(player, burn1, burn2);
    }

    private TextureRegion createMissileTexture ( Texture spriteSheet )
    {
        return new TextureRegion(spriteSheet, 120, 55, MISSILE_WIDTH, MISSILE_HEIGHT);
    }

    private LaserTextures createLaserTextures1 ( Texture spriteSheet )
    {
        TextureRegion a = new TextureRegion(spriteSheet, 112, 0, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion b = new TextureRegion(spriteSheet, 121, 0, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion c = new TextureRegion(spriteSheet, 130, 0, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion d = new TextureRegion(spriteSheet, 139, 0, LASER_WIDTH, LASER_HEIGHT);
        return new LaserTextures(a, b, c, d);
    }

    private LaserTextures createLaserTextures2 ( Texture spriteSheet )
    {
        TextureRegion a = new TextureRegion(spriteSheet, 112, 9, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion b = new TextureRegion(spriteSheet, 121, 9, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion c = new TextureRegion(spriteSheet, 130, 9, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion d = new TextureRegion(spriteSheet, 139, 9, LASER_WIDTH, LASER_HEIGHT);
        return new LaserTextures(a, b, c, d);
    }

    private LaserTextures createLaserTextures3 ( Texture spriteSheet )
    {
        TextureRegion a = new TextureRegion(spriteSheet, 112, 18, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion b = new TextureRegion(spriteSheet, 121, 18, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion c = new TextureRegion(spriteSheet, 130, 18, LASER_WIDTH, LASER_HEIGHT);
        TextureRegion d = new TextureRegion(spriteSheet, 139, 18, LASER_WIDTH, LASER_HEIGHT);
        return new LaserTextures(a, b, c, d);
    }

    private TextureRegion createHitTexture ( Texture spriteSheet )
    {
        return new TextureRegion(spriteSheet, 17, 45, HIT_WIDTH, HIT_HEIGHT);
    }



    public AlienTextures getAlienTextures1 ()
    {
        return alienTextures1;
    }

    public AlienTextures getAlienTextures2 ()
    {
        return alienTextures2;
    }

    public AlienTextures getAlienTextures3 ()
    {
        return alienTextures3;
    }

    public PlayerTextures getPlayerTextures ()
    {
        return playerTextures;
    }

    public TextureRegion getMissileTexture ()
    {
        return missileTexture;
    }

    public LaserTextures getLaserTextures1 ()
    {
        return laserTextures1;
    }

    public LaserTextures getLaserTextures2 ()
    {
        return laserTextures2;
    }

    public LaserTextures getLaserTextures3 ()
    {
        return laserTextures3;
    }

    public TextureRegion getHitTexture ()
    {
        return hitTexture;
    }

}
