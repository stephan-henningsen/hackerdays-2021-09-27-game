package com.trifork.hackerdays.game.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Animation
{

    private TextureRegion[] textures;
    private float[] durations;

    private float elapsedFrameTime = 0;
    private int currentFrame = 0;

    public Animation ( TextureRegion texture1, float duration1, TextureRegion texture2, float duration2 )
    {
        textures = new TextureRegion[]{texture1, texture2};
        durations = new float[]{duration1, duration2};
    }

    public Animation ( TextureRegion texture1, float duration1, TextureRegion texture2, float duration2, TextureRegion texture3, float duration3 )
    {
        textures = new TextureRegion[]{texture1, texture2, texture3};
        durations = new float[]{duration1, duration2, duration3};
    }

    public Animation ( TextureRegion texture1, float duration1, TextureRegion texture2, float duration2, TextureRegion texture3, float duration3, TextureRegion texture4, float duration4 )
    {
        textures = new TextureRegion[]{texture1, texture2, texture3, texture4};
        durations = new float[]{duration1, duration2, duration3, duration4};
    }

    public void update ( float dt )
    {
        elapsedFrameTime += dt;
        if ( elapsedFrameTime >= durations[currentFrame] ) {

            currentFrame++;
            if ( currentFrame == textures.length ) {
                currentFrame = 0;
            }

            elapsedFrameTime = elapsedFrameTime % durations[currentFrame];
        }
    }

    public TextureRegion getFrameTextureRegion ()
    {
        return textures[currentFrame];
    }


    public void divDurations ( float div )
    {
        for ( int i = 0 ; i < durations.length ; i++ ) {
            durations[i] /= div;
        }
    }
}
