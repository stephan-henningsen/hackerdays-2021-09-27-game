package com.trifork.hackerdays.game.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.trifork.hackerdays.game.sprite.Sprite;

public class Missile extends Sprite
{

    private static final int SPEED = 500;

    private final TextureRegion textureRegion;

    public Missile ( TextureRegion textureRegion, float x, float y, int width, int height )
    {
        super(x, y, width, height);
        this.textureRegion = textureRegion;
    }

    @Override
    public void update ( float dt )
    {
        y += SPEED * dt;
    }

    @Override
    public TextureRegion getTextureRegion ()
    {
        return textureRegion;
    }



    //
    //    ^
    //    |
    //  y |
    //    +----->
    //      x
    //            +-----+
    //            |     |
    //            |  #  |
    //            +--#--+
    //               #
    //               #
    //
    public boolean isColliding ( Alien alien )
    {
        float alienBottom = alien.getY();
        float alienTop = alienBottom + alien.getHeight();
        float alienLeft =  alien.getX();
        float alienRight = alienLeft + alien.getWidth();

        float missileX = getX();
        float missileBottom = getY();
        float missileTop = missileBottom + getHeight();

        return missileX >= alienLeft && missileX <= alienRight && missileTop >= alienBottom && missileBottom <= alienTop;
    }
}
