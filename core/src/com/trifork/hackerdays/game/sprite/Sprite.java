package com.trifork.hackerdays.game.sprite;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class Sprite
{

    protected float x, y;
    protected int width, height;



    protected Sprite ( float x, float y, int width, int height )
    {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }



    public int getX ()
    {
        return (int)x;
    }

    public void setX ( float x )
    {
        this.x = x;
    }

    public void addX ( float add )
    {
        x += add;
    }

    public void moveRight ( float add, int max )
    {
        addX(add);
        if ( x  > max ) {
            x = max;
        }
    }

    public void moveLeft ( float add, int min )
    {
        addX(add);
        if ( x  < min ) {
            x = min;
        }
    }



    public int getY ()
    {
        return (int)y;
    }

    public void setY ( float y )
    {
        this.y = y;
    }

    public void addY ( float add )
    {
        y += add;
    }



    public int getWidth ()
    {
        return width;
    }

    public int getHeight ()
    {
        return height;
    }



    public abstract void update ( float dt );

    public abstract TextureRegion getTextureRegion ();



    public boolean isAbove ( int limit )
    {
        return y - height > limit;
    }
}
