package com.trifork.hackerdays.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.trifork.hackerdays.game.screen.TitleScreen;
import com.trifork.hackerdays.game.sprite.SpriteFactory;
import com.trifork.hackerdays.game.sprite.cache.TextureCache;

public class SpaceInvadersGame extends Game
{
    public Preferences prefs;

    public static final int TOP = 40;
    public static final int BORDER = 10;
    public static final int ROW_HEIGHT = 10;

    public static final int GAME_WIDTH = 160;
    public static final int GAME_HEIGHT = 256;

    private Screen titleScreen;
    private Screen gameScreen;

    private Camera camera;
    private Viewport viewport;

    public BitmapFont font;
    private Texture spriteSheet;
    public TextureCache textureCache;
    public SpriteFactory spriteFactory;

    public SpriteBatch batch;

    public int level = 1;
    public int score = 0;
    public int lives = 3;
    public int highScore = 0;

    @Override
    public void create ()
    {
        camera = new OrthographicCamera();
        viewport = new FitViewport(SpaceInvadersGame.GAME_WIDTH, SpaceInvadersGame.GAME_HEIGHT, camera);
        viewport.apply();
        camera.position.set((float)SpaceInvadersGame.GAME_HEIGHT/2, (float)SpaceInvadersGame.GAME_WIDTH/2, 0);
        titleScreen = new TitleScreen(this);
        font = new BitmapFont();
        font.getData().setScale(.5f);
        font.setFixedWidthGlyphs("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 !.-=)");
        prefs = Gdx.app.getPreferences("game preferences");

        spriteSheet = new Texture("sprites.png");
        textureCache = new TextureCache(spriteSheet);
        spriteFactory = new SpriteFactory(textureCache);

        loadPersistedValues();

        batch = new SpriteBatch();

        setScreen(titleScreen);
    }

    private void loadPersistedValues() {
        highScore = prefs.getInteger("highscore", 0);
    }

    @Override
    public void render ()
    {
        super.render();
    }

    @Override
    public void resize ( int width, int height )
    {
        viewport.update(width, height, true);
        batch.setProjectionMatrix(camera.combined);
        camera.position.set((float)GAME_HEIGHT/2, (float)GAME_WIDTH/2, 0);
    }

    @Override
    public void dispose ()
    {
        batch.dispose();
        font.dispose();
        spriteSheet.dispose();
        getScreen().dispose();
    }

}
